#!/bin/sh

set -e

generate_password() {
    tr -dc 'A-Za-z0-9!#$%&*+-.=@' < /dev/urandom | head -c 13
}

awkenvsubst() {
    awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3);gsub("[$]{"var"}",ENVIRON[var])}}1'
}

echo "[burger-maker] pre apply script: create a MySQL database if does not exist"

if kubectl get service/${environment_name}-db 2> /dev/null
then
    echo "MySQL database already exists"
else
    echo "MySQL database not found: create"
    # generate random passwords
    export mysql_root_password=$(generate_password | base64 -)
    export mysql_password=$(generate_password | base64 -)

    # envsubst and apply
    awkenvsubst < mysql-deployment.yml | kubectl apply -f -
fi
