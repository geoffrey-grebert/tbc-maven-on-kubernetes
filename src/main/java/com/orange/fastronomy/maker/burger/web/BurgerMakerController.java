/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */
package com.orange.fastronomy.maker.burger.web;

import com.orange.fastronomy.maker.burger.domain.Burger;
import com.orange.fastronomy.maker.burger.domain.Purchase;
import com.orange.fastronomy.maker.burger.service.BurgerMakerService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.Instant;

@Path("/api/burger")
@Tag(name = "Burger maker API", description = "This API delivers burgers.")
@Produces(MediaType.APPLICATION_JSON)
public class BurgerMakerController {

    @Inject
    protected BurgerMakerService burgerMakerService;

    @Operation(description = "Deliver a burger and logs the purchase")
    @APIResponse(responseCode = "200", description = "Burger successfully delivered")
    @APIResponse(responseCode = "503", description = "No burger available")
    @DELETE()
    @Transactional
    @Path("/top")
    public Burger topBurger() {
        Burger burger = burgerMakerService.deliver()
                .orElseThrow(() -> new NoBurgerException("Can not deliver a burger"));
        Purchase purchase = new Purchase(Instant.now(), burger);
        purchase.persist();
        return burger;
    }

    @Operation(description = "Returns purchase logs, sorted by date (descending)")
    @GET()
    @Path("/purchases")
    public PaginatedResponse<Purchase> findPurchases(
            @Parameter(description = "pagination page number (zero-based)")
            @QueryParam("page")
            @DefaultValue("0")
                    int page,
            @Parameter(description = "pagination page size")
            @QueryParam("size")
            @DefaultValue("10")
                    int size
    ) {
        return new PaginatedResponse(Purchase.findAll().page(page, size));
    }

}
